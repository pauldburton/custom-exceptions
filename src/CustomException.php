<?php

/*
 * The MIT License
 *
 * Copyright 2015 Paul Burton.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace CustomExceptions;

use CustomExceptions\Exception;

/**
 * @author Paul Burton
 */
abstract class CustomException extends \Exception implements Exception
{

    protected $message = 'Unknown exception';     // Exception message
    protected $code = 0;                       // User-defined exception code
    protected $file;                              // Source filename of exception
    protected $line;                              // Source line of exception
    private $uuid;                              // Unique error identifier
    private $string;                            // Unknown
    private $trace;                             // Unknown

    public function __construct($message, $code = 0, $previous = null)
    {
        $this->uuid = uniqid();
        parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
        $errorString = get_class($this) . ":  {$this->message}' in {$this->file}({$this->line}) UUID: " . strval($this->uuid) . " {$this->getTraceAsString()}";

        if ($this->getPrevious() != NULL) {
            $errorString .= " \n Caused by: {$this->getPrevious()}";
        }

        return $errorString;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

}
